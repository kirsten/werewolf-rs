//! A Mapper implementation for [RocksDB](https://rocksdb.org/)
use rocksdb::{self, DB};
use std::error::Error;

use crate::data::{self, Game, Player};
use crate::mapper::Mapper;

#[derive(Debug)]
pub struct RocksDbMapper {
    conn: DB,
}

impl RocksDbMapper {
    pub fn new(path: &str) -> Result<RocksDbMapper, rocksdb::Error> {
        let conn = DB::open_default(path)?;
        Ok(RocksDbMapper { conn })
    }
}

impl Mapper<Game, Box<dyn Error>> for RocksDbMapper {
    fn get(&self, obj_id: uuid::Uuid) -> Result<Game, Box<dyn Error>> {
        let xs = match self.conn.get(obj_id) {
            Ok(Some(xs)) => xs,
            Ok(None) => return Err(Box::new(data::Error::GameNotFound { game_id: obj_id })),
            Err(e) => return Err(Box::new(e)),
        };
        let ys = String::from_utf8(xs)?;
        Ok(serde_json::from_str::<Game>(ys.as_str())?)
    }

    fn put(&self, obj: &Game) -> Result<(), Box<dyn Error>> {
        let ys = serde_json::to_string(obj)?;
        Ok(self.conn.put(obj.game_id(), ys)?)
    }

    fn rem(&self, obj_id: uuid::Uuid) -> Result<(), Box<dyn Error>> {
        Ok(self.conn.delete(obj_id)?)
    }
}

impl Mapper<Player, Box<dyn Error>> for RocksDbMapper {
    fn get(&self, obj_id: uuid::Uuid) -> Result<Player, Box<dyn Error>> {
        let xs = match self.conn.get(obj_id) {
            Ok(Some(xs)) => xs,
            Ok(None) => return Err(Box::new(data::Error::PlayerNotFound { player_id: obj_id })),
            Err(e) => return Err(Box::new(e)),
        };
        let ys = String::from_utf8(xs)?;
        Ok(serde_json::from_str::<Player>(ys.as_str())?)
    }

    fn put(&self, obj: &Player) -> Result<(), Box<dyn Error>> {
        let ys = serde_json::to_string(obj)?;
        Ok(self.conn.put(obj.player_id(), ys)?)
    }

    fn rem(&self, obj_id: uuid::Uuid) -> Result<(), Box<dyn Error>> {
        Ok(self.conn.delete(obj_id)?)
    }
}
