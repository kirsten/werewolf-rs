//! Data mappers in the form of key-value stores

use im::HashMap;
use std::cell::RefCell;
use uuid::Uuid;

use crate::data::{self, Game, Player};

pub mod rocksdb;

pub trait Mapper<T, E> {
    fn get(&self, obj_id: uuid::Uuid) -> Result<T, E>;
    fn put(&self, obj: &T) -> Result<(), E>;
    fn rem(&self, obj_id: uuid::Uuid) -> Result<(), E>;
}

/// A simple in-memory mapper
///
/// ## Examples
/// 
/// ```rust
/// use werewolf::{self, Game};
/// use werewolf::mapper::{HashMapMapper, Mapper};
/// 
/// let game = Game::new();
/// let mapper = HashMapMapper::new();
/// 
/// assert_eq!(mapper.put(&game), Ok(()));
/// assert_eq!(mapper.get(game.game_id()), Ok(game.clone()));
/// assert_eq!(mapper.rem(game.game_id()), Ok(()));
/// assert_eq!(
///     mapper.get(game.game_id()),
///     Err(werewolf::Error::GameNotFound {
///         game_id: game.game_id()
///     })
/// );
/// ```
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct HashMapMapper<T> {
    underlying: RefCell<HashMap<Uuid, T>>,
}

impl<T> HashMapMapper<T> {
    pub fn new() -> HashMapMapper<T> {
        HashMapMapper {
            underlying: RefCell::new(HashMap::new()),
        }
    }
}

impl<T> Default for HashMapMapper<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl Mapper<Game, data::Error> for HashMapMapper<Game> {
    fn get(&self, obj_id: uuid::Uuid) -> Result<Game, data::Error> {
        match self.underlying.borrow().get(&obj_id) {
            Some(obj) => Ok(obj.clone()),
            None => Err(data::Error::GameNotFound { game_id: obj_id }),
        }
    }

    fn put(&self, obj: &Game) -> Result<(), data::Error> {
        self.underlying
            .borrow_mut()
            .insert(obj.game_id(), obj.clone());
        Ok(())
    }

    fn rem(&self, obj_id: uuid::Uuid) -> Result<(), data::Error> {
        self.underlying.borrow_mut().remove(&obj_id);
        Ok(())
    }
}

impl Mapper<Player, data::Error> for HashMapMapper<Player> {
    fn get(&self, obj_id: uuid::Uuid) -> Result<Player, data::Error> {
        match self.underlying.borrow().get(&obj_id) {
            Some(obj) => Ok(obj.clone()),
            None => Err(data::Error::PlayerNotFound { player_id: obj_id }),
        }
    }

    fn put(&self, obj: &Player) -> Result<(), data::Error> {
        self.underlying
            .borrow_mut()
            .insert(obj.player_id(), obj.clone());
        Ok(())
    }

    fn rem(&self, obj_id: uuid::Uuid) -> Result<(), data::Error> {
        self.underlying.borrow_mut().remove(&obj_id);
        Ok(())
    }
}
