//! Data types, as immutable as I know to make them

pub mod game;
#[doc(inline)]
pub use crate::data::game::Game;

pub mod error;
#[doc(inline)]
pub use crate::data::error::Error;

pub mod player;
#[doc(inline)]
pub use crate::data::player::Player;

pub mod role;
#[doc(inline)]
pub use crate::data::role::Role;

pub mod turn;
#[doc(inline)]
pub use crate::data::turn::Turn;
