//! The game's turns
use serde::{Deserialize, Serialize};

/// The [Game](enum@crate::data::Game) is played
/// in alternating cycles of Day and Night.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Turn {
    /// When the Villagers banish.
    Day,
    /// When the Werewolves kill.
    Night,
}

impl Iterator for Turn {
    type Item = Turn;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            Turn::Day => Some(Turn::Night),
            Turn::Night => Some(Turn::Day),
        }
    }
}

impl DoubleEndedIterator for Turn {
    fn next_back(&mut self) -> Option<Self::Item> {
        match self {
            Turn::Day => Some(Turn::Night),
            Turn::Night => Some(Turn::Day),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn iter() {
        assert_eq!(Turn::Day.next(), Some(Turn::Night));
        assert_eq!(Turn::Day.next_back(), Some(Turn::Night));

        assert_eq!(Turn::Night.next(), Some(Turn::Day));
        assert_eq!(Turn::Night.next_back(), Some(Turn::Day));
    }
}
