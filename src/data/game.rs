//! The game struct and implementations.
//!
//! This module coordinates the game, though it could be better.
use im::{HashMap, OrdSet};
use rand::{seq::SliceRandom, Rng};
use serde::{Deserialize, Serialize};
use tracing::{debug, info, span, trace, Level};
use uuid::Uuid;

use crate::data::{Player, Role, Turn};

/// The game.
///
/// A game is an identifier,
/// a mapping of [Players](enum@crate::data::Player) to [Roles](enum@crate::data::Role),
/// and the current [Turn](enum@crate::data::Turn).
#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Game {
    /// initial state
    NotStarted { game_id: Uuid, players: Vec<Uuid> },
    /// While playing, the [Game] tracks the active players and the current turn
    Playing {
        game_id: Uuid,
        roles: HashMap<Uuid, Role>,
        turn: Turn,
    },
    /// After a [Game] is won, you can still see the winning team
    Won { game_id: Uuid, team: Role },
}

impl Game {
    /// constructor
    pub fn new() -> Game {
        Game::NotStarted {
            game_id: Uuid::new_v4(),
            players: Vec::new(),
        }
    }

    /// gets the identifier
    pub fn game_id(&self) -> Uuid {
        match self {
            Self::NotStarted {
                game_id,
                players: _,
            } => *game_id,
            Self::Playing {
                game_id,
                roles: _,
                turn: _,
            } => *game_id,
            Self::Won { game_id, team: _ } => *game_id,
        }
    }

    /// gets the active players
    pub fn players(&self) -> Vec<Uuid> {
        match self {
            Self::NotStarted {
                game_id: _,
                players,
            } => players.clone(),
            Self::Playing {
                game_id: _,
                roles,
                turn: _,
            } => roles.keys().cloned().collect(),
            Self::Won {
                game_id: _,
                team: _,
            } => Vec::new(),
        }
    }

    /// gets the active players with their roles
    pub fn roles(&self) -> HashMap<Uuid, Role> {
        match self {
            Self::NotStarted {
                game_id: _,
                players: _,
            } => HashMap::new(),
            Self::Playing {
                game_id: _,
                roles,
                turn: _,
            } => roles.clone(),
            Self::Won {
                game_id: _,
                team: _,
            } => HashMap::new(),
        }
    }

    /// A [Player] joins the game.
    ///
    /// Joining a game doesn't immediately assign a role.
    /// Instead, roles are assigned once all players are joined.
    pub fn join(&self, player_id: &Uuid) -> Game {
        match self {
            Self::NotStarted { game_id, players } => {
                let xs = {
                    let mut ys = players.clone();
                    ys.push(*player_id);
                    ys
                };
                Game::NotStarted {
                    game_id: *game_id,
                    players: xs,
                }
            }
            _ => self.clone(),
        }
    }

    /// Starts the [Game]
    ///
    /// Assigns roles to players and sets the first turn ([Night](type@crate::data::Turn::Night))
    pub fn start<R: Rng + ?Sized>(&self, rng: &mut R) -> Game {
        match self {
            Self::NotStarted { game_id, players } if players.len() > 4 => Game::Playing {
                game_id: *game_id,
                roles: assign_roles(players, rng),
                turn: Turn::Night,
            },
            _ => self.clone(),
        }
    }

    /// Coordinates the whole turn ([Day](type@crate::data::Turn::Day) or [Night](type@crate::data::Turn::Night)).
    pub fn take_turn<R: Rng + ?Sized>(
        &self,
        rng: &mut R,
        players: &[Player],
    ) -> (Game, Vec<Player>) {
        match (&self.winner(), &self) {
            (
                Some(team),
                Self::Playing {
                    game_id,
                    roles: _,
                    turn: _,
                },
            ) => (
                Game::Won {
                    game_id: *game_id,
                    team: *team,
                },
                players.to_vec(),
            ),
            (
                None,
                Self::Playing {
                    game_id,
                    roles,
                    turn,
                },
            ) => match turn {
                Turn::Day => do_banishing(game_id, roles, rng, players),
                Turn::Night => {
                    let visioned = do_seeing(rng, players);
                    do_killing(game_id, roles, rng, &visioned)
                }
            },
            _ => (self.clone(), players.to_vec()),
        }
    }

    /// Shows the current score,
    /// i.e. the sum of [Players](enum@crate::data::Player) by [Role].
    pub fn scoreboard(&self) -> HashMap<Role, u32> {
        match self {
            Self::Playing {
                game_id: _,
                roles,
                turn: _,
            } => roles.values().fold(HashMap::new(), |acc, role| {
                acc.update_with_key(*role, 1, |_, i, j| i + j)
            }),
            _ => HashMap::new(),
        }
    }

    /// Have we won yet?
    ///
    /// Returns `None` until a winner can be determined.
    ///
    /// * [Villagers](type@crate::data::Role::Villager) win
    ///   when all werewolves have been banished.
    /// * [Werewolves](type@crate::data::Role::Werewolf) win
    ///   when they equal or exceed the number of villagers.
    pub fn winner(&self) -> Option<Role> {
        match self {
            Self::Playing {
                game_id: _,
                roles: _,
                turn: _,
            } => {
                let scores: HashMap<Role, u32> = self.scoreboard();
                let team_werewolf = scores.get(&Role::Werewolf).cloned().unwrap_or(0);
                let team_villager = scores
                    .get(&Role::Villager)
                    .cloned()
                    .map(|v| scores.get(&Role::Seer).cloned().map_or(v, |s| v + s))
                    .unwrap_or(0);

                debug!(
                    "current score is: Werewolves {:?}, Villagers {:?}",
                    team_werewolf, team_villager
                );

                if team_werewolf == 0 {
                    Some(Role::Villager)
                } else if team_werewolf >= team_villager {
                    Some(Role::Werewolf)
                } else {
                    None
                }
            }
            Self::Won { game_id: _, team } => Some(*team),
            _ => None,
        }
    }
}

impl Default for Game {
    fn default() -> Self {
        Self::new()
    }
}

/// Assign roles to [Players](enum@crate::data::Player).
///
/// The relationship of werewolves to non-werewolves is
/// the `sqrt(x)` to `x`.
/// This produces suboptimal results,
/// e.g., if there are 4 players, the Werewolves immediately win
/// (see [winner](fn@Game::winner) below).
/// So it goes.
fn assign_roles<R: Rng + ?Sized>(players: &Vec<Uuid>, rng: &mut R) -> HashMap<Uuid, Role> {
    let player_num = players.len() as f64;
    let mut roles: HashMap<Uuid, Role> = HashMap::new();

    let mut seers_remaining: u8 = 1;
    let mut wolves_remaining = player_num.sqrt().floor() as u64;
    let mut shuffled: Vec<Uuid> = players.clone();
    shuffled.shuffle(rng);

    for player_id in shuffled {
        if seers_remaining > 0 {
            debug!("player-{:?} is a seer", player_id);
            roles.insert(player_id, Role::Seer);
            seers_remaining -= 1;
        } else if wolves_remaining > 0 {
            debug!("player-{:?} is a werewolf", player_id);
            roles.insert(player_id, Role::Werewolf);
            wolves_remaining -= 1;
        } else {
            roles.insert(player_id, Role::Villager);
        }
    }
    roles
}

/// During the [Day](type@crate::data::Turn::Day) turn.
///
/// * Gather and process each player's hit list.
/// * Transition turn to [Night](type@crate::data::Turn::Night).
/// * Inform each [Player] of the victim.
fn do_banishing<R: Rng + ?Sized>(
    game_id: &Uuid,
    roles: &HashMap<Uuid, Role>,
    rng: &mut R,
    players: &[Player],
) -> (Game, Vec<Player>) {
    let _span = span!(Level::TRACE, "banishing").entered();
    let hit_lists: Vec<(Player, Option<Vec<Uuid>>)> = players
        .iter()
        .map(|player| (player.clone(), player.banish(rng)))
        .collect();
    let victim = get_victim(&hit_lists);
    match victim {
        Some((victim_id, victim_role, killers)) => {
            info!("A {} was banished by an angry mob!", victim_role);
            (
                Game::Playing {
                    game_id: *game_id,
                    roles: roles.without(&victim_id),
                    turn: Turn::Night, // next turn is Night
                },
                players
                    .iter()
                    .map(|player| player.banished(&victim_id, &victim_role, &killers))
                    .collect(),
            )
        }
        None => (
            Game::Playing {
                game_id: *game_id,
                roles: roles.clone(),
                turn: Turn::Night,
            },
            players.to_vec(),
        ),
    }
}

/// During the [Night](type@crate::data::Turn::Night) turn.
///
/// * Gather and process the seer's hit list.
/// * Does not transition turn.
/// * Inform each [Seer](type@crate::data::Role::Seer) of the vision.
fn do_seeing<R: Rng + ?Sized>(rng: &mut R, players: &[Player]) -> Vec<Player> {
    let _span = span!(Level::TRACE, "seeing").entered();
    let vision_lists: Vec<(Player, Option<Vec<Uuid>>)> = players
        .iter()
        .map(|player| (player.clone(), player.see(rng)))
        .collect();
    match get_victim(&vision_lists) {
        Some((subject_id, subject_role, _)) => {
            info!("A {} was seen by the seer!", subject_role);
            players
                .iter()
                .map(|player| player.saw(&subject_id, &subject_role))
                .collect()
        }
        _ => players.to_vec(),
    }
}

/// During the [Night](type@crate::data::Turn::Night) turn.
///
/// * Gather and process each werewolf's hit list.
/// * Transition turn to [Day](type@crate::data::Turn::Day).
/// * Inform each [Player] of the victim.
fn do_killing<R: Rng + ?Sized>(
    game_id: &Uuid,
    roles: &HashMap<Uuid, Role>,
    rng: &mut R,
    players: &[Player],
) -> (Game, Vec<Player>) {
    let _span = span!(Level::TRACE, "killing").entered();
    let kill_lists: Vec<(Player, Option<Vec<Uuid>>)> = players
        .iter()
        .map(|player| (player.clone(), player.kill(rng)))
        .collect();
    let victim = get_victim(&kill_lists);
    match victim {
        Some((victim_id, victim_role, killers)) => {
            info!("A {} was killed by werewolves!", victim_role);
            (
                Game::Playing {
                    game_id: *game_id,
                    roles: roles.without(&victim_id),
                    turn: Turn::Day, // next turn is Day
                },
                players
                    .iter()
                    .map(|player| player.killed(&victim_id, &victim_role, &killers))
                    .collect(),
            )
        }
        None => (
            Game::Playing {
                game_id: *game_id,
                roles: roles.clone(),
                turn: Turn::Day,
            },
            players.to_vec(),
        ),
    }
}

/// Performs a crude [rank-choice voting](https://en.wikipedia.org/wiki/Ranked_voting) algorithm.
///
/// Given each [Player's](enum@crate::data::Player) "hit list",
/// merges them into a final list,
/// then takes the highest-ranked player as the victim.
fn rank_choice(xss: &Vec<(Player, Option<Vec<Uuid>>)>) -> Option<Uuid> {
    // Count of unique candidates
    let candidate_count = xss
        .iter()
        .filter_map(|(_, oxs)| oxs.as_ref().map(|xs| OrdSet::from(xs.clone())))
        .reduce(|x, y| x.union(y))
        .map_or(0, |ys| ys.len());
    debug!("rank-choice voting with {:?} candidates", candidate_count);

    // Rank the candidates
    let mut unsorted: HashMap<Uuid, i32> = HashMap::new();
    for (_, xs) in xss {
        let ys = xs.clone().unwrap_or(vec![]);
        for (i, x) in ys.into_iter().enumerate() {
            match unsorted.get(&x) {
                Some(j) => unsorted.insert(x, ((candidate_count - i) as i32) + j),
                None => unsorted.insert(x, (candidate_count - i) as i32),
            };
        }
    }
    trace!("unsorted results: {:?}", unsorted);

    // Sort candidates and return the top
    let sorted = {
        let mut sorted: Vec<(Uuid, i32)> = unsorted.into_iter().collect();
        sorted.sort_by_key(|(_, i)| *i * -1);
        sorted
    };
    sorted.first().map(|(chosen, _)| *chosen)
}

/// The player who died, their role before death, and the people who voted to kill them.
fn get_victim(xss: &Vec<(Player, Option<Vec<Uuid>>)>) -> Option<(Uuid, Role, Vec<Uuid>)> {
    let victim_id = rank_choice(xss);

    let victim_role = xss
        .iter()
        .find(|(player, _)| Some(player.player_id()) == victim_id)
        .and_then(|(player, _)| player.role());

    let murderers = xss
        .iter()
        .filter_map(|(player, xs)| {
            if victim_id.and_then(|x| xs.clone().map(|ys| ys.contains(&x))) == Some(true) {
                Some(player.player_id())
            } else {
                None
            }
        })
        .collect();
    debug!(
        "the victim was: player-{:?}, a {:?}",
        victim_id, victim_role
    );

    victim_id.and_then(|id| victim_role.map(|r| (id, r, murderers)))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn werewolf_role_is_sqrt_of_total() {
        let mut rng = rand::thread_rng();

        // A 5-player game
        // floor(sqrt(x))
        let game5 = Game::new()
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .start(&mut rng);
        assert_eq!(game5.scoreboard().get(&Role::Werewolf).cloned(), Some(2));
        // does not meet the win condition
        assert_eq!(game5.winner(), None);

        let game9 = Game::new()
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .join(&Uuid::new_v4())
            .start(&mut rng);
        assert_eq!(game9.scoreboard().get(&Role::Werewolf).cloned(), Some(3));
        // does not meet the win condition
        assert_eq!(game9.winner(), None);
    }

    #[test]
    fn ranked_choice_finds_best_candidate() {
        let candidates = vec![
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
        ];

        // No voters
        let mut ballots = Vec::new();
        assert_eq!(rank_choice(&ballots), None);

        // No ballots cast
        ballots = vec![
            (Player::new(), None),
            (Player::new(), None),
            (Player::new(), None),
        ];
        assert_eq!(rank_choice(&ballots), None);

        // All ballots agree on first choice
        ballots = vec![
            (
                Player::new(),
                Some(vec![candidates[0], candidates[1], candidates[4]]),
            ),
            (
                Player::new(),
                Some(vec![candidates[0], candidates[2], candidates[4]]),
            ),
            (
                Player::new(),
                Some(vec![candidates[0], candidates[3], candidates[4]]),
            ),
        ];
        assert_eq!(rank_choice(&ballots), Some(candidates[0]));

        // All ballots agree on second choice
        ballots = vec![
            (
                Player::new(),
                Some(vec![candidates[1], candidates[0], candidates[4]]),
            ),
            (
                Player::new(),
                Some(vec![candidates[2], candidates[0], candidates[4]]),
            ),
            (
                Player::new(),
                Some(vec![candidates[3], candidates[0], candidates[4]]),
            ),
        ];
        assert_eq!(rank_choice(&ballots), Some(candidates[0]));

        // Most ballots agree on first choice
        ballots = vec![
            (
                Player::new(),
                Some(vec![candidates[0], candidates[1], candidates[4]]),
            ),
            (
                Player::new(),
                Some(vec![candidates[0], candidates[2], candidates[4]]),
            ),
            (
                Player::new(),
                Some(vec![candidates[3], candidates[0], candidates[4]]),
            ),
        ];
        assert_eq!(rank_choice(&ballots), Some(candidates[0]));

        // Most ballots agree on second choice
        ballots = vec![
            (
                Player::new(),
                Some(vec![candidates[0], candidates[1], candidates[4]]),
            ),
            (
                Player::new(),
                Some(vec![candidates[2], candidates[0], candidates[4]]),
            ),
            (
                Player::new(),
                Some(vec![candidates[3], candidates[0], candidates[4]]),
            ),
        ];
        assert_eq!(rank_choice(&ballots), Some(candidates[0]));
    }
}
