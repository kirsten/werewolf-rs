//! The player's roles
use serde::{Deserialize, Serialize};
use std::fmt;

/// The [Player's](enum@crate::data::Player) role.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Role {
    /// The Seer (or Detective in the original game).
    Seer,
    /// The Werewolf (or Mafioso in the original game).
    Werewolf,
    /// The "innocent" Villager (or Citizen in the original).
    Villager,
}

impl fmt::Display for Role {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
