//! The player struct and implementations.
//!
//! Each player is "trustworthy" in that,
//! while they have access to more information than they are allowed,
//! they refuse to use it.
use im::{HashMap, OrdMap};
use rand::{seq::SliceRandom, Rng};
use serde::{Deserialize, Serialize};
use tracing::{debug, span, Level};
use uuid::Uuid;

use crate::data::Role;

/// The amount to weight a suspect when they are a known werewolf
const KNOWN_WEREWOLF: i16 = i16::MAX;
/// The increment for some suspicion
const LIGHT_SUS: i16 = 0xf;
/// The increment for a great deal of suspicion
const HEAVY_SUS: i16 = 0xff;

/// The player.
///
/// Players have an identifier, a [Role], and a list of suspects.
/// The `suspects` is a map of the suspect's identifier
/// and a "suspicion rating".
/// More on this later.
#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum Player {
    /// initial state
    NotStarted { player_id: Uuid },
    /// while playing, a [Player] has a role and a list of suspects
    Playing {
        player_id: Uuid,
        role: Role,
        suspects: HashMap<Uuid, i16>,
    },
    /// a [Player] can be killed by werewolves
    Killed { player_id: Uuid, role: Role },
    /// a [Player] can be banished by an angry mob
    Banished { player_id: Uuid, role: Role },
}

impl Player {
    /// constructor
    pub fn new() -> Player {
        Player::NotStarted {
            player_id: Uuid::new_v4(),
        }
    }

    /// gets the identifier
    pub fn player_id(&self) -> Uuid {
        match self {
            Self::NotStarted { player_id } => *player_id,
            Self::Playing {
                player_id,
                role: _,
                suspects: _,
            } => *player_id,
            Self::Killed { player_id, role: _ } => *player_id,
            Self::Banished { player_id, role: _ } => *player_id,
        }
    }

    /// gets the current role
    pub fn role(&self) -> Option<Role> {
        match self {
            Self::Playing {
                player_id: _,
                role,
                suspects: _,
            } => Some(*role),
            Self::Killed { player_id: _, role } => Some(*role),
            Self::Banished { player_id: _, role } => Some(*role),
            _ => None,
        }
    }

    /// gets the current suspect list (if any)
    pub fn suspects(&self) -> HashMap<Uuid, i16> {
        match self {
            Self::Playing {
                player_id: _,
                role: _,
                suspects,
            } => suspects.clone(),
            _ => HashMap::new(),
        }
    }

    /// Assign a [Role] to this player and create suspect list.
    ///
    /// Suspects start with a suspicion rating of zero.
    pub fn assign_role(&self, roles: &HashMap<Uuid, Role>) -> Player {
        match (&self, roles.get(&self.player_id())) {
            (Self::NotStarted { player_id }, Some(role)) => {
                Player::Playing {
                    player_id: *player_id,
                    role: *role,
                    suspects: HashMap::from_iter(roles.keys().filter_map(|id| {
                        if id == player_id {
                            None // we never suspect ourselves of being the villain
                        } else {
                            Some((*id, 0))
                        }
                    })),
                }
            }
            _ => self.clone(),
        }
    }

    /// During the [Day](type@crate::data::Turn::Day), the townsfolk are driven to a frenzy.
    ///
    /// All living players participate in daytime scape-goating.
    pub fn banish<R: Rng + ?Sized>(&self, rng: &mut R) -> Option<Vec<Uuid>> {
        match self {
            Self::Playing {
                player_id: _,
                role: _,
                suspects,
            } => Some(hit_list(suspects, rng)),
            _ => None,
        }
    }

    /// During the [Night](type@crate::data::Turn::Night), the beasts howl and hunt.
    ///
    /// Only werewolves hunt at night.
    pub fn kill<R: Rng + ?Sized>(&self, rng: &mut R) -> Option<Vec<Uuid>> {
        match self {
            Self::Playing {
                player_id: _,
                role: Role::Werewolf,
                suspects,
            } => Some(hit_list(suspects, rng)),
            _ => None,
        }
    }

    /// During the [Night](type@crate::data::Turn::Night), the seer sees.
    ///
    /// Seer visions only come at night.
    pub fn see<R: Rng + ?Sized>(&self, rng: &mut R) -> Option<Vec<Uuid>> {
        match self {
            Self::Playing {
                player_id: _,
                role: Role::Seer,
                suspects,
            } => Some(
                hit_list(suspects, rng)
                    .into_iter()
                    .filter(|player_id| {
                        match suspects.get(player_id) {
                            Some(&i) => i < KNOWN_WEREWOLF, // filter out known werewolves
                            None => true,
                        }
                    })
                    .collect(),
            ),
            _ => None,
        }
    }

    /// The inevitable news that you banished the wrong person
    ///
    /// Your suspicion raises if:
    /// * You voted to banish a seer (Team Villager)
    /// * You voted to banish a werewolf (Team Werewolf)
    /// * You did not vote to banish a villager or seer (Team Werewolf)
    ///
    /// Your suspicion lowers if:
    /// * You did not vote to banish a villager or seer (Team Villager)
    ///
    /// Your suspicion is removed if:
    /// * You are dead
    pub fn banished(&self, banished_id: &Uuid, banished_role: &Role, mob: &[Uuid]) -> Player {
        match self {
            Self::Playing {
                player_id,
                role,
                suspects: _,
            } if banished_id == player_id => Player::Banished {
                player_id: *player_id,
                role: *role,
            },
            Self::Playing {
                player_id,
                role,
                suspects,
            } => banished(player_id, role, suspects, banished_id, banished_role, mob),
            _ => self.clone(),
        }
    }

    /// The inevitable news that a villager was killed during the night
    ///
    /// Your suspicion is removed if:
    /// * You are dead
    /// * You are a fellow werewolf
    pub fn killed(&self, killed_id: &Uuid, killed_role: &Role, wolves: &[Uuid]) -> Player {
        match self {
            Self::Playing {
                player_id,
                role,
                suspects: _,
            } if killed_id == player_id => Player::Killed {
                player_id: *player_id,
                role: *role,
            },
            Self::Playing {
                player_id,
                role,
                suspects,
            } => killed(player_id, role, suspects, killed_id, killed_role, wolves),
            _ => self.clone(),
        }
    }

    /// The seer's vision
    ///
    /// Your suspicion raises to the max if:
    /// * You are a werewolf
    ///
    /// Your suspicion is removed if:
    /// * You are not a werewolf
    pub fn saw(&self, subject_id: &Uuid, subject_role: &Role) -> Player {
        match self {
            Self::Playing {
                player_id,
                role,
                suspects,
            } => saw(player_id, role, suspects, subject_id, subject_role),
            _ => self.clone(),
        }
    }
}

impl Default for Player {
    fn default() -> Self {
        Self::new()
    }
}

/// Creates a player's hit list
///
/// Starting with the suspects list:
/// * group them by current suspicion rating
/// * randomly shuffle each group
/// * flatten into a list with most suspicious first
fn hit_list<R: Rng + ?Sized>(suspects: &HashMap<Uuid, i16>, rng: &mut R) -> Vec<Uuid> {
    // group suspects by suspicion rating
    // shuffle order for equal ratings
    let mut grouped: OrdMap<i16, Vec<Uuid>> = OrdMap::new();
    for (&suspect, &weight) in suspects.iter() {
        if weight < 0 {
            continue;
        }
        let value: Vec<Uuid> = match grouped.get_mut(&weight) {
            Some(xs) => {
                xs.push(suspect);
                xs.to_vec()
            }
            None => vec![suspect],
        };
        grouped.insert(weight, value);
    }

    // return sorted by most suspicious
    grouped
        .values()
        .rev()
        .flat_map(|xs| {
            let mut ys = xs.clone();
            ys.shuffle(rng);
            ys
        })
        .collect()
}

fn banished(
    player_id: &Uuid,
    role: &Role,
    suspects: &HashMap<Uuid, i16>,
    banished_id: &Uuid,
    banished_role: &Role,
    mob: &[Uuid],
) -> Player {
    let _span = span!(Level::TRACE, "banishing");
    let suspects = HashMap::from_iter(suspects.iter().filter_map(|(&suspect, &weight)| {
        match (role, banished_role, mob.contains(&suspect)) {
            _ if &suspect == banished_id => {
                // remove the deceased
                None
            }
            _ if weight == KNOWN_WEREWOLF => {
                // once a werewolf, always a werewolf
                Some((suspect, weight))
            }
            (Role::Villager, Role::Villager, false) => {
                // voting against banishing a villager is good for Team Villager
                debug!(
                    "player-{:?} :: player-{:?} -{:?}",
                    player_id, suspect, LIGHT_SUS
                );
                Some((suspect, weight - LIGHT_SUS))
            }
            (Role::Seer, Role::Villager, false) => {
                // voting against banishing a villager is good for Team Villager
                debug!(
                    "player-{:?} :: player-{:?} -{:?}",
                    player_id, suspect, LIGHT_SUS
                );
                Some((suspect, weight - LIGHT_SUS))
            }
            (Role::Villager, Role::Seer, false) => {
                // voting against banishing a seer is very good for Team Villager
                debug!(
                    "player-{:?} :: player-{:?} -{:?}",
                    player_id, suspect, HEAVY_SUS
                );
                Some((suspect, weight - HEAVY_SUS))
            }
            (Role::Seer, Role::Seer, false) => {
                // voting against banishing a seer is very good for Team Villager
                debug!(
                    "player-{:?} :: player-{:?} -{:?}",
                    player_id, suspect, HEAVY_SUS
                );
                Some((suspect, weight - HEAVY_SUS))
            }
            (Role::Villager, Role::Seer, true) => {
                // banishing a seer is very bad for Team Villager
                debug!(
                    "player-{:?} :: player-{:?} +{:?}",
                    player_id, suspect, HEAVY_SUS
                );
                Some((suspect, weight + HEAVY_SUS))
            }
            (Role::Seer, Role::Seer, true) => {
                // banishing a seer is very bad for Team Villager
                debug!(
                    "player-{:?} :: player-{:?} +{:?}",
                    player_id, suspect, HEAVY_SUS
                );
                Some((suspect, weight + HEAVY_SUS))
            }
            (Role::Werewolf, Role::Werewolf, true) => {
                // banishing a werewolf is very bad for Team Werewolf
                debug!(
                    "player-{:?} :: player-{:?} +{:?}",
                    player_id, suspect, HEAVY_SUS
                );
                Some((suspect, weight + HEAVY_SUS))
            }
            (Role::Werewolf, Role::Werewolf, false) => {
                // not banishing a werewolf is ok for Team Werewolf
                Some((suspect, weight))
            }
            (Role::Werewolf, _, false) => {
                // not banishing a non-werewolf is bad for Team Werewolf
                debug!(
                    "player-{:?} :: player-{:?} +{:?}",
                    player_id, suspect, LIGHT_SUS
                );
                Some((suspect, weight + LIGHT_SUS))
            }
            _ => {
                // otherwise, no change
                Some((suspect, weight))
            }
        }
    }));
    Player::Playing {
        player_id: *player_id,
        role: *role,
        suspects,
    }
}

fn killed(
    player_id: &Uuid,
    role: &Role,
    suspects: &HashMap<Uuid, i16>,
    killed_id: &Uuid,
    killed_role: &Role,
    wolves: &[Uuid],
) -> Player {
    let suspects = HashMap::from_iter(suspects.iter().filter_map(|(&suspect, &weight)| {
        match (&role, &killed_role, wolves.contains(&suspect)) {
            _ if &suspect == killed_id => None, // remove the deceased
            (Role::Werewolf, _, true) => None,  // fellow werewolves are beyond suspicion
            _ => Some((suspect, weight)),       // otherwise, no change
        }
    }));
    Player::Playing {
        player_id: *player_id,
        role: *role,
        suspects,
    }
}

fn saw(
    player_id: &Uuid,
    role: &Role,
    suspects: &HashMap<Uuid, i16>,
    subject_id: &Uuid,
    subject_role: &Role,
) -> Player {
    let suspects = HashMap::from_iter(suspects.iter().filter_map(|(&suspect, &weight)| {
        match &subject_role {
            Role::Werewolf if &suspect == subject_id => Some((suspect, KNOWN_WEREWOLF)),
            _ if &suspect == subject_id => None, // any other role is no longer suspect
            _ => Some((suspect, weight)),        // otherwise, no change
        }
    }));
    Player::Playing {
        player_id: *player_id,
        role: *role,
        suspects,
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use im::hashmap;

    #[test]
    fn assigning_a_role_also_creates_suspect_list() {
        let mut player = Player::new();
        let roles: HashMap<Uuid, Role> = hashmap! {
            player.player_id() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
        };
        player = player.assign_role(&roles);
        assert_eq!(player.role(), Some(Role::Villager));
        assert_eq!(
            player.suspects(),
            roles
                .iter()
                .filter_map(|(p_id, _)| {
                    if p_id == &player.player_id() {
                        None
                    } else {
                        Some((p_id.clone(), 0))
                    }
                })
                .collect::<HashMap<Uuid, i16>>()
        );
    }

    #[test]
    fn only_werewolves_may_kill() {
        let mut rng = rand::thread_rng();
        let player_id = Uuid::new_v4();
        let mut roles: HashMap<Uuid, Role> = hashmap! {
            player_id => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
        };
        let mut player = Player::NotStarted { player_id }.assign_role(&roles);
        assert_eq!(player.kill(&mut rng), None);

        roles = hashmap! {
            player_id => Role::Werewolf,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
        };
        player = Player::NotStarted { player_id }.assign_role(&roles);
        assert_eq!(player.kill(&mut rng).map(|xs| xs.len()), Some(4));

        roles = hashmap! {
            player_id => Role::Seer,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
        };
        player = Player::NotStarted { player_id }.assign_role(&roles);
        assert_eq!(player.kill(&mut rng).map(|xs| xs.len()), None);
    }

    #[test]
    fn only_survivors_may_banish() {
        let mut rng = rand::thread_rng();
        let player_id = Uuid::new_v4();
        let mut roles: HashMap<Uuid, Role> = hashmap! {
            player_id => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
        };
        let mut player = Player::NotStarted { player_id }.assign_role(&roles);
        assert_eq!(player.banish(&mut rng).map(|xs| xs.len()), Some(4));

        roles = hashmap! {
            player_id => Role::Werewolf,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
        };
        player = Player::NotStarted { player_id }.assign_role(&roles);
        assert_eq!(player.banish(&mut rng).map(|xs| xs.len()), Some(4));

        roles = hashmap! {
            player_id => Role::Seer,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
        };
        player = Player::NotStarted { player_id }.assign_role(&roles);
        assert_eq!(player.banish(&mut rng).map(|xs| xs.len()), Some(4));
    }

    #[test]
    fn only_seers_may_see() {
        let mut rng = rand::thread_rng();
        let player_id = Uuid::new_v4();
        let mut roles: HashMap<Uuid, Role> = hashmap! {
            player_id => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
        };
        let mut player = Player::NotStarted { player_id }.assign_role(&roles);
        assert_eq!(player.see(&mut rng), None);

        roles = hashmap! {
            player_id => Role::Werewolf,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
        };
        player = Player::NotStarted { player_id }.assign_role(&roles);
        assert_eq!(player.see(&mut rng).map(|xs| xs.len()), None);

        roles = hashmap! {
            player_id => Role::Seer,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
            Uuid::new_v4() => Role::Villager,
        };
        player = Player::NotStarted { player_id }.assign_role(&roles);
        assert_eq!(player.see(&mut rng).map(|xs| xs.len()), Some(4));
    }

    #[test]
    fn hit_list_ordering() {
        let mut rng = rand::thread_rng();
        let suspects = hashmap! {
            Uuid::new_v4() => 0,
            Uuid::new_v4() => 2,
            Uuid::new_v4() => 1,
            Uuid::new_v4() => 3,
            Uuid::new_v4() => 2,
        };

        let hit_list = hit_list(&suspects, &mut rng);

        assert_eq!(hit_list.len(), 5);
        assert_eq!(suspects.get(&hit_list[0]).cloned(), Some(3));
        assert_eq!(suspects.get(&hit_list[1]).cloned(), Some(2));
        assert_eq!(suspects.get(&hit_list[2]).cloned(), Some(2));
        assert_eq!(suspects.get(&hit_list[3]).cloned(), Some(1));
        assert_eq!(suspects.get(&hit_list[4]).cloned(), Some(0));
    }

    #[test]
    fn villager_suspicions() {
        let mut suspects = vec![
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
        ];
        let player = Player::Playing {
            player_id: Uuid::new_v4(),
            role: Role::Villager,
            suspects: suspects
                .iter()
                .map(|id| (id.clone(), 0))
                .collect::<HashMap<Uuid, i16>>(),
        };
        let abstainer = suspects.pop().unwrap();
        suspects.push(player.player_id());

        let with_dead_villager = player.banished(&suspects[0], &Role::Villager, &suspects);
        assert_eq!(
            with_dead_villager.suspects(),
            hashmap! {
                // (suspects[0], 0),          deceased
                suspects[1] => 0,
                suspects[2] => 0,
                suspects[3] => 0,
                suspects[4] => 0,
                abstainer   => -LIGHT_SUS, // abstained
            }
        );

        let with_dead_seer = player.banished(&suspects[0], &Role::Seer, &suspects);
        assert_eq!(
            with_dead_seer.suspects(),
            hashmap! {
                // (suspects[0], 0),          deceased
                suspects[1] => HEAVY_SUS,
                suspects[2] => HEAVY_SUS,
                suspects[3] => HEAVY_SUS,
                suspects[4] => HEAVY_SUS,
                abstainer   => -HEAVY_SUS, // abstained
            }
        );

        let with_dead_werewolf = player
            .clone()
            .banished(&suspects[0], &Role::Werewolf, &suspects);
        assert_eq!(
            with_dead_werewolf.suspects(),
            hashmap! {
                // (suspects[0], 0),          deceased
                suspects[1] => 0,
                suspects[2] => 0,
                suspects[3] => 0,
                suspects[4] => 0,
                abstainer   => 0,          // abstained
            }
        );
    }

    #[test]
    fn seer_suspicions() {
        let mut suspects = vec![
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
        ];
        let player = Player::Playing {
            player_id: Uuid::new_v4(),
            role: Role::Seer,
            suspects: suspects
                .iter()
                .map(|id| (id.clone(), 0))
                .collect::<HashMap<Uuid, i16>>(),
        };
        let abstainer = suspects.pop().unwrap();
        suspects.push(player.player_id());

        let with_dead_villager = player
            .clone()
            .banished(&suspects[0], &Role::Villager, &suspects);
        assert_eq!(
            with_dead_villager.suspects(),
            hashmap! {
                // (suspects[0], 0),          deceased
                suspects[1] => 0,
                suspects[2] => 0,
                suspects[3] => 0,
                suspects[4] => 0,
                abstainer   => -LIGHT_SUS, // abstained
            }
        );

        let with_dead_seer = player.banished(&suspects[0], &Role::Seer, &suspects);
        assert_eq!(
            with_dead_seer.suspects(),
            hashmap! {
                // (suspects[0], 0),          deceased
                suspects[1] => HEAVY_SUS,
                suspects[2] => HEAVY_SUS,
                suspects[3] => HEAVY_SUS,
                suspects[4] => HEAVY_SUS,
                abstainer   => -HEAVY_SUS, // abstained
            }
        );

        let with_dead_werewolf = player
            .clone()
            .banished(&suspects[0], &Role::Werewolf, &suspects);
        assert_eq!(
            with_dead_werewolf.suspects(),
            hashmap! {
                // (suspects[0], 0),          deceased
                suspects[1] => 0,
                suspects[2] => 0,
                suspects[3] => 0,
                suspects[4] => 0,
                abstainer   => 0,          // abstained
            }
        );
    }

    #[test]
    fn werewolf_suspicions() {
        let mut suspects = vec![
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
            Uuid::new_v4(),
        ];
        let player = Player::Playing {
            player_id: Uuid::new_v4(),
            role: Role::Werewolf,
            suspects: suspects
                .iter()
                .map(|id| (id.clone(), 0))
                .collect::<HashMap<Uuid, i16>>(),
        };
        let abstainer = suspects.pop().unwrap();
        suspects.push(player.player_id());

        let with_dead_villager = player
            .clone()
            .banished(&suspects[0], &Role::Villager, &suspects);
        assert_eq!(
            with_dead_villager.suspects(),
            hashmap! {
                // (suspects[0], 0),          deceased
                suspects[1] => 0,
                suspects[2] => 0,
                suspects[3] => 0,
                suspects[4] => 0,
                abstainer   => LIGHT_SUS,  // abstained
            }
        );

        let with_dead_seer = player.banished(&suspects[0], &Role::Seer, &suspects);
        assert_eq!(
            with_dead_seer.suspects(),
            hashmap! {
                // (suspects[0], 0),          deceased
                suspects[1] => 0,
                suspects[2] => 0,
                suspects[3] => 0,
                suspects[4] => 0,
                abstainer   => LIGHT_SUS,  // abstained
            }
        );

        let with_dead_werewolf = player
            .clone()
            .banished(&suspects[0], &Role::Werewolf, &suspects);
        assert_eq!(
            with_dead_werewolf.suspects(),
            hashmap! {
                // (suspects[0], 0),          deceased
                suspects[1] => HEAVY_SUS,
                suspects[2] => HEAVY_SUS,
                suspects[3] => HEAVY_SUS,
                suspects[4] => HEAVY_SUS,
                abstainer   => 0,          // abstained
            }
        );
    }
}
