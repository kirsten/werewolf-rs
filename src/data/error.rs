//! Custom error types
use serde::{Deserialize, Serialize};
use std::error;
use std::fmt;

/// Custom error types for more useful log messages
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Error {
    GameNotFound { game_id: uuid::Uuid },
    PlayerNotFound { player_id: uuid::Uuid },
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Error::GameNotFound { game_id } => {
                write!(f, "Game {} not found", game_id)
            }
            Error::PlayerNotFound { player_id } => {
                write!(f, "Player {} not found", player_id)
            }
        }
    }
}

impl error::Error for Error {}
