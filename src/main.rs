use clap::{Args, Parser, Subcommand};
use rand::prelude::*;
use rand_chacha::ChaCha8Rng;
use std::error::Error;
use tracing::{debug, error, info, Level};
use tracing_subscriber::FmtSubscriber;
use uuid::Uuid;

use werewolf::mapper::rocksdb::RocksDbMapper;
use werewolf::mapper::Mapper;
use werewolf::{Game, Player, Role};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
#[command(propagate_version = true)]
struct Cli {
    /// Do not use ANSI colors in the output
    #[arg(short, long, action)]
    no_color: bool,

    /// Show debug output
    /// (show trace output if added twice)
    #[arg(short, long, action = clap::ArgAction::Count)]
    verbose: u8,

    #[command(subcommand)]
    command: Option<Commands>,
}

#[derive(Subcommand)]
enum Commands {
    /// Play a game of Werewolf
    Play(PlayArgs),
    /// Show the result of a specific game
    ShowGame(ShowGameArgs),
    /// Show the result for a specific plyer
    ShowPlayer(ShowPlayerArgs),
}

#[derive(Args)]
struct PlayArgs {
    /// Specify the number of players,
    /// leave unset for a random amount (5-30)
    #[arg(short, long)]
    players: Option<u8>,

    /// Initial RNG seed
    #[arg(short, long)]
    seed: Option<u64>,
}

#[derive(Args)]
struct ShowGameArgs {
    /// The Game identifier
    #[arg(short, long)]
    game_id: Uuid,
}

#[derive(Args)]
struct ShowPlayerArgs {
    /// The Player identifier
    #[arg(short, long)]
    player_id: Uuid,
}

/// The game
fn play(
    player_count: Option<u8>,
    seed: Option<u64>,
    games_db: &impl Mapper<Game, Box<dyn Error>>,
    players_db: &impl Mapper<Player, Box<dyn Error>>,
) -> Result<(), Box<dyn Error>> {
    // setup rng
    let mut rng = seed
        .map(ChaCha8Rng::seed_from_u64)
        .unwrap_or(ChaCha8Rng::from_entropy());

    // initial state
    let player_count: u8 = player_count.unwrap_or(rng.gen_range(5..30));
    let mut players: Vec<Player> = (0..player_count).map(|_| Player::new()).collect();
    let mut game = players
        .iter()
        .fold(Game::new(), |acc, p| acc.join(&p.player_id()));

    debug!(
        "created game-{:?} with {:?} players",
        game.game_id(),
        game.players().len()
    );

    // assign roles
    game = game.start(&mut rng);
    let werewolf_count = game.scoreboard().get(&Role::Werewolf).map_or(0, |i| *i);
    players = players
        .iter()
        .map(|player| player.assign_role(&game.roles()))
        .collect();

    info!("Playing Werewolf with {player_count} players, {werewolf_count} of whom are werewolves!");
    games_db.put(&game)?;
    for player in players.iter() {
        players_db.put(player)?;
    }

    let mut winner = game.winner();
    while winner.is_none() {
        (game, players) = game.take_turn(&mut rng, &players);
        winner = game.winner();
    }
    // Take one more turn to change the ADT
    (game, players) = game.take_turn(&mut rng, &players);

    info!(
        "Team {:?} won!",
        winner.expect("game has ended with no winner")
    );
    games_db.put(&game)?;
    for player in players.iter() {
        players_db.put(player)?;
    }

    Ok(())
}

/// Show the result of a specific game
fn show_game(
    game_id: Uuid,
    games_db: &impl Mapper<Game, Box<dyn Error>>,
) -> Result<(), Box<dyn Error>> {
    match games_db.get(game_id)?.winner() {
        Some(winner) => info!("Team {} won!", winner),
        None => error!("No one has won this game!"),
    }
    Ok(())
}

/// Show the result for a specific player
fn show_player(
    player_id: Uuid,
    players_db: &impl Mapper<Player, Box<dyn Error>>,
) -> Result<(), Box<dyn Error>> {
    match players_db.get(player_id)?.role() {
        Some(role) => info!("Player was a {}!", role),
        None => error!("This player was never assigned a role!"),
    }
    Ok(())
}

/// Entry point to the application
fn main() -> Result<(), Box<dyn Error>> {
    let cli = Cli::parse();

    // setup databases
    let games_db = RocksDbMapper::new("games.rocks")?;
    let players_db = RocksDbMapper::new("plyers.rocks")?;

    // setup logging
    let subscriber = FmtSubscriber::builder()
        .with_max_level(match cli.verbose {
            0 => Level::INFO,
            1 => Level::DEBUG,
            _ => Level::TRACE,
        })
        .with_ansi(!cli.no_color)
        .finish();
    tracing::subscriber::set_global_default(subscriber)?;

    match &cli.command {
        Some(Commands::Play(args)) => play(args.players, args.seed, &games_db, &players_db),
        Some(Commands::ShowGame(args)) => show_game(args.game_id, &games_db),
        Some(Commands::ShowPlayer(args)) => show_player(args.player_id, &players_db),
        None => play(None, None, &games_db, &players_db),
    }
}
