#![doc = include_str!("../README.md")]

pub mod data;
#[doc(inline)]
pub use crate::data::Error;
#[doc(inline)]
pub use crate::data::Game;
#[doc(inline)]
pub use crate::data::Player;
#[doc(inline)]
pub use crate::data::Role;
#[doc(inline)]
pub use crate::data::Turn;

pub mod mapper;
