# Werewolf

A zero-player version of the social deduction game
designed by [Dimitry Davidoff](https://en.wikipedia.org/wiki/Mafia_(party_game))
with rules modifications by [Andrew Plotkin](https://www.eblong.com/zarf/werewolf.html).

This is an excuse for me to explore Rust fundamentals.
If you absolutely must run it yourself, download the repo, and:

```bash
cargo run
```

Then, to see the documentation:

```bash
cargo docs --open
```

And if you're into tests:

```bash
cargo test
```

## Sample Run

```text
2023-08-08T16:42:25.706025Z  INFO werewolf: Playing Werewolf with 28 players, 5 of whom are werewolves!
2023-08-08T16:42:25.708080Z  INFO werewolf::data::game: A Villager was seen by the seer!
2023-08-08T16:42:25.709059Z  INFO werewolf::data::game: A Villager was killed by werewolves!
2023-08-08T16:42:25.711644Z  INFO werewolf::data::game: A Villager was banished by an angry mob!
2023-08-08T16:42:25.712440Z  INFO werewolf::data::game: A Villager was seen by the seer!
2023-08-08T16:42:25.713135Z  INFO werewolf::data::game: A Villager was killed by werewolves!
2023-08-08T16:42:25.715262Z  INFO werewolf::data::game: A Villager was banished by an angry mob!
2023-08-08T16:42:25.715929Z  INFO werewolf::data::game: A Villager was seen by the seer!
2023-08-08T16:42:25.716485Z  INFO werewolf::data::game: A Villager was killed by werewolves!
2023-08-08T16:42:25.718095Z  INFO werewolf::data::game: A Villager was banished by an angry mob!
2023-08-08T16:42:25.718569Z  INFO werewolf::data::game: A Villager was seen by the seer!
2023-08-08T16:42:25.718986Z  INFO werewolf::data::game: A Villager was killed by werewolves!
2023-08-08T16:42:25.720161Z  INFO werewolf::data::game: A Villager was banished by an angry mob!
2023-08-08T16:42:25.720576Z  INFO werewolf::data::game: A Villager was seen by the seer!
2023-08-08T16:42:25.720881Z  INFO werewolf::data::game: A Villager was killed by werewolves!
2023-08-08T16:42:25.721767Z  INFO werewolf::data::game: A Villager was banished by an angry mob!
2023-08-08T16:42:25.722066Z  INFO werewolf::data::game: A Villager was seen by the seer!
2023-08-08T16:42:25.722295Z  INFO werewolf::data::game: A Villager was killed by werewolves!
2023-08-08T16:42:25.722945Z  INFO werewolf::data::game: A Villager was banished by an angry mob!
2023-08-08T16:42:25.723166Z  INFO werewolf::data::game: A Villager was seen by the seer!
2023-08-08T16:42:25.723324Z  INFO werewolf::data::game: A Seer was killed by werewolves!
2023-08-08T16:42:25.723744Z  INFO werewolf::data::game: A Villager was banished by an angry mob!
2023-08-08T16:42:25.723930Z  INFO werewolf::data::game: A Villager was killed by werewolves!
2023-08-08T16:42:25.724206Z  INFO werewolf::data::game: A Villager was banished by an angry mob!
2023-08-08T16:42:25.724517Z  INFO werewolf::data::game: A Werewolf was banished by an angry mob!
2023-08-08T16:42:25.724790Z  INFO werewolf::data::game: A Werewolf was banished by an angry mob!
2023-08-08T16:42:25.725014Z  INFO werewolf::data::game: A Werewolf was banished by an angry mob!
2023-08-08T16:42:25.725210Z  INFO werewolf::data::game: A Werewolf was banished by an angry mob!
2023-08-08T16:42:25.725376Z  INFO werewolf::data::game: A Werewolf was banished by an angry mob!
2023-08-08T16:42:25.725437Z  INFO werewolf: Team Villager won!
```
